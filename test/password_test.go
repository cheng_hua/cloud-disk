package test

import (
	"cloud-disk/core/helper"
	"fmt"
	"testing"
)

func TestPassordCheck(t *testing.T) {
	password := "123456"
	passwordHash, err := helper.HashPassword(password)
	if nil != err {
		t.Fatal(err)
	}
	fmt.Printf("%s passwordHash is %s\n", password, passwordHash)
	err = helper.CheckPassword(password, string(passwordHash))
	if nil != err {
		fmt.Printf("checkPassword failed %s", err.Error())
		t.Fail()
	} else {
		fmt.Println("checkPasword success")
	}
	errorPassword := "1234567"
	err = helper.CheckPassword(errorPassword, string(passwordHash))
	if nil == err {
		fmt.Println("checke Error Password failed")
		t.Fail()
	} else {
		fmt.Println("check Error Password success")
	}
}
