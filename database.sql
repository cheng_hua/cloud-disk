create database cloud_disk with owner postgres;

\c cloud_disk;

create table user_basic (
    id serial not null primary key,
    identity varchar(36) not null,
    name varchar(60) not null,
    password varchar(255) not null,
    email varchar(255) not null,
    created_at timestamptz(6),
    updated_at timestamptz(6),
    deleted_at timestamptz(6)
);

create table repository_pool (
    id serial not null primary key,
    identity varchar(36),
    hash varchar(255),
    name varchar(255),
    ext varchar(30),
    size  double precision,
    path varchar(255),
    created_at timestamptz(6),
    updated_at timestamptz(6)
);

create table user_repository (
    id serial not null primary key,
    identity varchar(36),
    user_identity varchar(36),
    parent_id int,
    repository_identity varchar(36),
    ext varchar(255),
    name varchar(255)
);

create table share_basic (
    id serial not null primary key,
    identity varchar(36),
    user_identity varchar(36),
    repository_identity varchar(36),
    expired_time int,
    created_at timestamptz(6),
    updated_at timestamptz(6),
    deleted_at timestamptz(6)
);