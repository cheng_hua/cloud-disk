package config

import "github.com/zeromicro/go-zero/rest"

type Config struct {
	rest.RestConf
	Postgres struct {
		UserName     string
		Password     string
		DatabaseName string
		Host         string
		Port         int
		SslMode      string
		MaxIdleConns int
		MaxOpenConns int
		ShowSql      bool
	}
	Redis struct {
		Addr      string
		Password  string
		DefaultDb int
	}
}
