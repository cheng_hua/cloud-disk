package logic

import (
	"context"
	"errors"

	"cloud-disk/core/internal/svc"
	"cloud-disk/core/internal/types"
	"cloud-disk/core/models"

	"github.com/zeromicro/go-zero/core/logx"
)

type UserDetailLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
}

func NewUserDetailLogic(ctx context.Context, svcCtx *svc.ServiceContext) *UserDetailLogic {
	return &UserDetailLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
	}
}

func (l *UserDetailLogic) UserDetail(req *types.UserDetailRequest) (resp *types.UserDetailResponse, err error) {
	// todo: add your logic here and delete this line
	ub := new(models.UserBasic)
	exists, err := models.Engine.Where("identity=?", req.Identity).Limit(1).Get(ub)
	if nil != err {
		return nil, err
	}
	if !exists {
		return nil, errors.New("User not exists")
	}
	resp = new(types.UserDetailResponse)
	resp.Name = ub.Name
	resp.Email = ub.Email
	return resp, nil
}
