package logic

import (
	"context"
	"errors"

	"cloud-disk/core/helper"
	"cloud-disk/core/internal/svc"
	"cloud-disk/core/internal/types"
	"cloud-disk/core/models"

	"github.com/zeromicro/go-zero/core/logx"
)

type UserLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
}

func NewUserLogic(ctx context.Context, svcCtx *svc.ServiceContext) *UserLogic {
	return &UserLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
	}
}

func (l *UserLogic) User(req *types.LoginRequest) (resp *types.LoginResponse, err error) {
	// todo: add your logic here and delete this line
	user := new(models.UserBasic)
	exists, err := models.Engine.Where("name =? and password =?", req.Name, req.Password).Get(user)
	if nil != err {
		return nil, err
	}
	if !exists {
		return nil, errors.New("User not exists")
	}
	token, err := helper.GenerateToken(uint64(user.Id), user.Identity, user.Name)
	if nil != err {
		return nil, err
	}
	resp = new(types.LoginResponse)
	resp.Token = token
	return resp, nil
}
