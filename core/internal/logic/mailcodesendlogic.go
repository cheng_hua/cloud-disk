package logic

import (
	"context"
	"time"

	"cloud-disk/core/helper"
	"cloud-disk/core/internal/svc"
	"cloud-disk/core/internal/types"
	"cloud-disk/core/models"

	"github.com/zeromicro/go-zero/core/logx"
)

type MailCodeSendLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
}

func NewMailCodeSendLogic(ctx context.Context, svcCtx *svc.ServiceContext) *MailCodeSendLogic {
	return &MailCodeSendLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
	}
}

func (l *MailCodeSendLogic) MailCodeSend(req *types.MailSendReq) (resp *types.MailSendResp, err error) {
	// todo: add your logic here and delete this line
	randCode := helper.RandomCode()
	err = helper.MailSendCode(req.Email, randCode)
	resp = new(types.MailSendResp)
	if nil != err {
		resp.Code = -1
		logx.Error(err.Error())
		err = nil
		return
	}
	key := req.Email + "_code"
	rStatus := models.RDB.SetEX(l.ctx, key, randCode, time.Second*300)
	if nil != rStatus.Err() {
		resp.Code = -2
		logx.Error(err.Error())
		err = nil
		return
	}
	resp.Code = 200
	return
}
