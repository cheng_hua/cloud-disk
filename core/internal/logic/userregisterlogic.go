package logic

import (
	"context"

	"cloud-disk/core/helper"
	"cloud-disk/core/internal/svc"
	"cloud-disk/core/internal/types"
	"cloud-disk/core/models"

	"github.com/zeromicro/go-zero/core/logx"
)

type UserRegisterLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
}

func NewUserRegisterLogic(ctx context.Context, svcCtx *svc.ServiceContext) *UserRegisterLogic {
	return &UserRegisterLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
	}
}

func (l *UserRegisterLogic) UserRegister(req *types.RegisterRequest) (resp *types.RegisterResponse, err error) {
	// todo: add your logic here and delete this line
	user := new(models.UserBasic)
	user.Email = req.Email
	user.Name = req.Name
	resp = new(types.RegisterResponse)
	resp.Code = -3
	passwordHash, err := helper.HashPassword(req.Password)
	if nil != err {
		return
	}
	user.Password = string(passwordHash)
	user.Identity = helper.GenNewUUID()
	count, err := models.Engine.InsertOne(user)

	if nil != err {
		resp.Code = -3
		resp.Msg = err.Error()
		return
	}
	logx.Info("%s insert %d %s", user.TableName(), count, user.Identity)
	resp.Code = 200
	return
}
