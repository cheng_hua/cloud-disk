package main

import (
	"flag"
	"fmt"

	"cloud-disk/core/internal/config"
	"cloud-disk/core/internal/handler"
	"cloud-disk/core/internal/svc"
	"cloud-disk/core/models"

	"github.com/zeromicro/go-zero/core/conf"
	"github.com/zeromicro/go-zero/rest"
)

var configFile = flag.String("f", "etc/core-api.yaml", "the config file")
var G_Config config.Config

func main() {
	flag.Parse()

	conf.MustLoad(*configFile, &G_Config)
	models.Init(G_Config.Postgres.UserName, G_Config.Postgres.Password, G_Config.Postgres.DatabaseName, G_Config.Postgres.Host,
		G_Config.Postgres.Port, G_Config.Postgres.SslMode, G_Config.Postgres.MaxIdleConns, G_Config.Postgres.MaxOpenConns)
	models.InitRedis(G_Config.Redis.Addr, G_Config.Redis.Password, G_Config.Redis.DefaultDb)
	models.Engine.ShowSQL(G_Config.Postgres.ShowSql)
	ctx := svc.NewServiceContext(G_Config)
	server := rest.MustNewServer(G_Config.RestConf)
	defer server.Stop()

	handler.RegisterHandlers(server, ctx)

	fmt.Printf("Starting server at %s:%d...\n", G_Config.Host, G_Config.Port)
	server.Start()
}
