package models

import "time"

type UserBasic struct {
	Id        int64
	Identity  string
	Name      string
	Password  string
	Email     string
	CreatedAt time.Time `xorm:"created 'created_at'"`
	UpdatedAt time.Time `xorm:"updated 'updated_at'"`
	DeletedAt time.Time `xorm:"deleted 'deleted_at'"`
}

func (table UserBasic) TableName() string {
	return "user_basic"
}
